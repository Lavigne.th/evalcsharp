﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eval_lib
{
    public class Counter
    {
        //Valeur courante du compteur
        private int number;

        public Counter(int start)
        {
            number = start;
        }

        /*
         * Retourne la valeur du compteur 
         */
        public int getNumber()
        {
            return number;
        }

        /*
         * Permet d'incrémenter la valeurs du compteur de 1 
         */
        public void increment()
        {
            number++;
        }

        /*
         * Permet décrémenter la valeurs du compteur de 1 
         */
        public void decrement()
        {
            if(number - 1 >= 0)
                number--;
        }

        /*
         * Remet la valeur du compteur à 0
         */
        public void reset()
        {
            number = 0;
        }
    }
}
