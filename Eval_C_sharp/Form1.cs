﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using eval_lib;

namespace Eval_C_sharp
{
    public partial class Form1 : Form
    {
        private Counter counter;

        public Form1()
        {
            InitializeComponent();
            counter = new Counter(0);
        }

        /*
         * Lorsque l'on clique sur le bouton pour décrémenter le compteur,
         * décrémente le compteur de 1
         */
        private void decrementBtn_Click(object sender, EventArgs e)
        {
            counter.decrement();
            labelScreen.Text = counter.getNumber().ToString();
        }

        /*
        * Lorsque l'on clique sur le bouton pour incrémenter le compteur,
        * incrémente le compteur de 1
        */
        private void incrementBtn_Click(object sender, EventArgs e)
        {
            counter.increment();
            labelScreen.Text = counter.getNumber().ToString();
        }

        /*
        * Lorsque l'on clique sur le bouton pour remetre a zero le compteur,
        * remet a 0 le compteur
        */
        private void razBtn_Click(object sender, EventArgs e)
        {
            counter.reset();
            labelScreen.Text = counter.getNumber().ToString();
        }
    }
}
