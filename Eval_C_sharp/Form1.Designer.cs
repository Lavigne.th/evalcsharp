﻿namespace Eval_C_sharp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.decrementBtn = new System.Windows.Forms.Button();
            this.incrementBtn = new System.Windows.Forms.Button();
            this.labelScreen = new System.Windows.Forms.Label();
            this.totalLabel = new System.Windows.Forms.Label();
            this.razBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // decrementBtn
            // 
            this.decrementBtn.Location = new System.Drawing.Point(217, 245);
            this.decrementBtn.Name = "decrementBtn";
            this.decrementBtn.Size = new System.Drawing.Size(80, 40);
            this.decrementBtn.TabIndex = 0;
            this.decrementBtn.Text = "-";
            this.decrementBtn.UseVisualStyleBackColor = true;
            this.decrementBtn.Click += new System.EventHandler(this.decrementBtn_Click);
            // 
            // incrementBtn
            // 
            this.incrementBtn.Location = new System.Drawing.Point(547, 245);
            this.incrementBtn.Name = "incrementBtn";
            this.incrementBtn.Size = new System.Drawing.Size(80, 40);
            this.incrementBtn.TabIndex = 1;
            this.incrementBtn.Text = "+";
            this.incrementBtn.UseVisualStyleBackColor = true;
            this.incrementBtn.Click += new System.EventHandler(this.incrementBtn_Click);
            // 
            // labelScreen
            // 
            this.labelScreen.AutoSize = true;
            this.labelScreen.Font = new System.Drawing.Font("Microsoft Sans Serif", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelScreen.Location = new System.Drawing.Point(367, 135);
            this.labelScreen.Name = "labelScreen";
            this.labelScreen.Size = new System.Drawing.Size(83, 91);
            this.labelScreen.TabIndex = 2;
            this.labelScreen.Text = "0";
            // 
            // totalLabel
            // 
            this.totalLabel.AutoSize = true;
            this.totalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalLabel.Location = new System.Drawing.Point(335, 78);
            this.totalLabel.Name = "totalLabel";
            this.totalLabel.Size = new System.Drawing.Size(109, 46);
            this.totalLabel.TabIndex = 3;
            this.totalLabel.Text = "Total";
            // 
            // razBtn
            // 
            this.razBtn.Location = new System.Drawing.Point(370, 299);
            this.razBtn.Name = "razBtn";
            this.razBtn.Size = new System.Drawing.Size(80, 40);
            this.razBtn.TabIndex = 4;
            this.razBtn.Text = "RAZ";
            this.razBtn.UseVisualStyleBackColor = true;
            this.razBtn.Click += new System.EventHandler(this.razBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.razBtn);
            this.Controls.Add(this.totalLabel);
            this.Controls.Add(this.labelScreen);
            this.Controls.Add(this.incrementBtn);
            this.Controls.Add(this.decrementBtn);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button decrementBtn;
        private System.Windows.Forms.Button incrementBtn;
        private System.Windows.Forms.Label labelScreen;
        private System.Windows.Forms.Label totalLabel;
        private System.Windows.Forms.Button razBtn;
    }
}

