﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using eval_lib;

namespace eval_test
{
    [TestClass]
    public class CounterTest
    {
        /*
         * Test de la fonction d'incrémentation de la classe Counter 
         */
        [TestMethod]
        public void incrementTest()
        {
            Counter counter = new Counter(0);
            Assert.AreEqual(0, counter.getNumber());

            counter.increment();
            Assert.AreEqual(1, counter.getNumber());
            counter.increment();
            Assert.AreEqual(2, counter.getNumber());
        }

        /*
         * Test de la fonction décrémentation de la classe Counter 
         */
        [TestMethod]
        public void decrementTest()
        {
            Counter counter = new Counter(0);
            Assert.AreEqual(0, counter.getNumber());

            counter.decrement();
            Assert.AreEqual(-1, counter.getNumber());
            counter.decrement();
            Assert.AreEqual(-2, counter.getNumber());
        }

        /*
         * Test de la fonction de remise à 0 du compteur
         */
        [TestMethod]
        public void razTest()
        {
            Counter counter = new Counter(0);
            Assert.AreEqual(0, counter.getNumber());

            counter.decrement();
            counter.reset();
            Assert.AreEqual(0, counter.getNumber());
        } 
    }
}
